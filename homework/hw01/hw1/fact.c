#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

//Fact
int calcFactorial(int x) {
    if (x == 0 || x == 1) {
        return 1;
    } else {
        return (x * calcFactorial(x - 1));
    }
}

int main(int argc, char **argv) {
    if (argc > 1) {
        int intVarient = atoi(argv[1]);
        float floatVarient = atof(argv[1]);

        if (floatVarient != intVarient || intVarient <= 0) {
            printf("Huh?");
        } else if (intVarient > 12) {
            printf("Overflow");
        } else {
            int factorial = calcFactorial(intVarient);
            printf("%i", factorial);
        }
    }
    return 0;
}