#include <assert.h>
#include <stdio.h>
#include <math.h>
#include "point.h"

/*
 * Update *p by increasing p->x by x and 
 * p->y by y
 */
void point_translate(Point *p, double x, double y) {
    p->x += x;
    p->y += y;
//    printf("Your job is to implement translate\n");
//    assert(0);
}

/*
 * Return the distance from p1 to p2
 */
double point_distance(const Point *p1, const Point *p2) {
    float x1, x2, y1, y2;
    x1 = p1->x;
    y1 = p1->y;
    x2 = p2->x;
    y2 = p2->y;
    double distance = sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2));
//    printf("Your job is to implement distance\n");
//    assert(0);
    return distance;
}
