/*  Name: Alex Oladele
    UniqueID: Oladelaa
    Program: Homework #2
*/

#include <iostream>
#include <fstream>
#include <unordered_map>
#include <algorithm>
#include <iterator>
#include <cstdio>
#include <cctype>
#include <vector>
#include <cstdlib>
#include <map>


using namespace std;
using StringMap = unordered_map<string, string>;
typedef pair<string, string> InputData;
StringMap phonebook;
map<string, string> orderedPhonebook;
char *roster;

// ========== HELPER METHODS ==========
//Reads in file that user provides
int readInContacts(char *filePath) {
    ifstream inFile;
    if (filePath == "") {
//        cout  << "Either no file provided, or file-name provided does not exist!";
        throw "Either no file provided, or file-name provided does not exist!";
    }

//    Opens the file
    inFile.open(filePath);

//    Makes sure that the file can be read before proceeding
    if (inFile.good()) {

//        Fills the unordered map with the file contents
        string readLine;
        int lineNum = 1;
        string key, value;
        while (getline(inFile, readLine)) {
            if (lineNum % 2 == 1) {
                key = readLine;
            }
            if (lineNum % 2 == 0) {
                value = readLine;
            }
            if (!key.empty() && !value.empty()) {
                InputData tempPair(key, value);
                phonebook.insert(tempPair);
                key = "";
                value = "";
            }
            lineNum++;
        }
        return 0;

    } else {
        throw "File could not be read!";
    }
}

//    Copy unordered map to ordered map because I don't mess with vectors
void copyMap(const InputData &insert) {
    orderedPhonebook.insert(insert);
}

//Prints out the ordered map's contents
void listMap(const InputData &data) {
    cout << data.first + "\t ==> " + data.second << endl;
}

// Writes the unorderedmap to a file
void writeToFile(InputData writeOut) {
    ofstream outFile;
    outFile.open(roster, ios::app);
    outFile << writeOut.first << "\n" << writeOut.second << endl;
    outFile.close();
}

// ========== MENU ACTIONS ==========
// Finds the specified contact
void doFind() {
//    Gets input name from user and looks for it in the hashmap
    string inputName;
    cout << "Name (e.g. Clinton, Bill): ";
    getline(cin, inputName);
//    As long as the user doesn't input an empty string, the command should return a result
    if (!inputName.empty()) {
        auto gotEmail = phonebook.find(inputName);

//        If the result is not found, then let the user know or else display the email to the user if found
        if (gotEmail == phonebook.end()) {
            cout << "Sorry, that name is not listed in the Email Address Book.\n\n" << endl;
        } else {
            cout << "\nEmail address for " + inputName + " is: " + gotEmail->second + "\n\n" << endl;
        }
    } else {
//        Message to output if the user doesn't supply a name
        cout << "No name is given to search for!\n\n" << endl;
    }
}

// Adds a new contact to the Email Address Book
void doAdd() {
//    Gets name and email form user to add to the hashmap
    string inputName, inputEmail;
    cout << "Name(e.g. Obama, Barack): ";
    getline(cin, inputName);
//    If the line isn't empty and doesn't already exist in the Email Address Book, then the get the email to put in hashmap
    if (!inputName.empty()) {
        cout << "Email for " + inputName + "(e.g. obamabh@miamioh.edu): ";
        getline(cin, inputEmail);
        if (!inputEmail.empty()) {
//            Stores data into hashmap
            InputData tempPair(inputName, inputEmail);
            phonebook.insert(tempPair);
            cout << endl;
            cout << "Successful insertion. \n\n" << endl;
        } else {
            cout << "No email supplied! Contact was not added to Email Address Book\n\n" << endl;
        }
    } else {
        cout << "No name supplied! Contact was not added to Email Address Book\n\n" << endl;
    }


}

//Edits an existing contact in Email Address Book
void doEdit() {
//    Makes sure the user supplied is actually in the map
    string inputName, newEmail;
    cout << "Name(e.g. Carter, Jimmy): ";
    getline(cin, inputName);
    auto foundContact = phonebook.find(inputName);
    if (foundContact != phonebook.end()) {
//        If user is in map, then proceed to get the new email address from them
        cout << "New email for " + inputName + ": ";
        getline(cin, newEmail);
        foundContact->second = newEmail;
        if (foundContact->second == newEmail) {
//            Makes sure that the list was updated successfully
            cout << "Successful modification\n\n" << endl;
        } else {
            cout << "Error occurred: Contact not updated!";
        }
    } else {
        cout << "Sorry, that contact does not exist in the roster.";
    }
}

//Saves the Email Address Book to a file
void doSave(char *filePath) {
//    Saves hashmap to same file (overwrite)
    ofstream saveFile;
    saveFile.open(filePath);
    for_each(phonebook.begin(), phonebook.end(), writeToFile);

    if (saveFile.good()) {
        cout << "Save successful.\n\n" << endl;
    }
}

//List all entries in map
void doList() {
    orderedPhonebook.clear();
    for_each(phonebook.begin(), phonebook.end(), copyMap);
    cout << endl;
    for_each(orderedPhonebook.begin(), orderedPhonebook.end(), listMap);
    cout << endl << endl;
}

//Processes what happens with the user input
void process(istream &in, ostream &out, char *fileName) {
    string cmd;
//    Menu for user to interact with
    do {
        out
                << "Welcome to the Email address book! \n\t1. Find email address\n\t2. List all entries in the Email Address Book\n\t"
                        "3. Modify entry in Email Address Book\n\t4. Insert new entry in Email Address Book\n\t5. Save Email Address Book to a file\n\t6. Quit program"
                << endl;
//        Reads in line from user
        out << "Enter your command: ";
        getline(in, cmd);

//        Determines what action the user wants to take
        if (cmd == "1") {
            doFind();
        } else if (cmd == "2") {
            doList();
        } else if (cmd == "3") {
            doEdit();
        } else if (cmd == "4") {
            doAdd();
        } else if (cmd == "5") {
            doSave(fileName);
        } else if (cmd == "6") {
            out << "Exiting the program...";
        } else {
            out << "INVALID COMMAND. PLEASE ENTER AN INTEGER BETWEEN 1 AND 5\n\n" << endl;
        }
    } while (cmd != "6");
}

int main(int argc, char **argv) {
//    exit 99 means that the commandline file was not provided or could not be read
    int status = 404;
    char *filePath;

    if (argc == 2) {
        filePath = argv[1];
        roster = argv[1];
        status = readInContacts(filePath);
    }
    if (status == 0) {
        process(cin, cout, filePath);
    } else {
        cout << "No File provided. Exiting" << endl;
        return 404;
    }
} 
