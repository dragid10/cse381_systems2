// Copyright 2014 raodm@miamioh.edu

#include <string>
#include "User.h"

User::User() {
    std::cout << "User::User() called.\n";
}

User::~User() {
    std::cout << "User::~User() called.\n";
}

User&
User::operator=(const User& other) {
    this->userID    = other.userID;
    this->firstName = other.firstName;
    this->lastName  = other.lastName;
    return *this;
}

User::User(const std::string& userID, const std::string& firstName,
           const std::string& lastName) : userID(userID), firstName(firstName),
                                          lastName(lastName) {
    // Nothing else to be done in the constructor
    std::cout << "User::User(...) called.\n";
}

User::User(const User& src) {
    userID    = src.userID;
    firstName = src.firstName;
    lastName  = src.lastName;
    std::cout << "User::User(const User& src) called.\n";
}

User::User(User&& src) {
    userID    = std::move(src.userID);
    firstName = std::move(src.firstName);
    lastName  = std::move(src.lastName);
    std::cout << "User::User(const User&& src) called.\n";
}

bool
User::operator<(const User& other) const {
    return (lastName == other.lastName) ? (firstName < other.firstName) :
        (lastName < other.lastName);
}

bool
User::operator==(const User& other) const {
    return (userID == other.userID);
}

bool
User::operator()(const User& other) const {
    return (this->firstName == other.firstName) ||
        (this->lastName == other.lastName) ||
        (this->userID   == other.userID);
}

std::ostream& operator<<(std::ostream& os, const User& u) {
    os << u.firstName << " " << u.lastName << " ("
       << u.userID   << ")";
    return os;
}

std::istream& operator>>(std::istream& is, User& u) {
    is >> u.lastName >> u.firstName >> u.userID;
    return is;
}
