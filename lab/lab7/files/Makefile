# Makefile for use with emacs and emacs's flymake mode
# Copyright (C) raodm@miamiOH.edu
#
# From emacs menu select Tools->compile
# To compile one file use the syntax:
#     make SRC=hello.cpp
# The compile many files use the syntax (change my_exe_name below):
#     make many EXE=my_exe_name
#

.PHONY: check-syntax all clean style many style-many

CXX=g++
CXXFLAGS=-Wall -g -Wextra -std=c++11
LIBS=
OPTS=
DEF_LIBS=-lm -lpthread

# Target exectuable name if SRC is defined
ifdef SRC
OBJ=$(patsubst %.cpp, %.o, $(SRC))
EXE=$(patsubst %.cpp, %,   $(SRC))
endif

# Variables to conditionally download cpplint.py
have_lint=$(wildcard cpplint.py)
ifneq ('$(have_lint)', 'cpplint.py')
WGET=get-lint
endif

all: build style

build: $(SRC)
ifeq (,$(findstring .h, $(SRC)))
	$(CXX) $(OPTS) $(CXXFLAGS) $(SRC) -o $(EXE) $(LIBS) $(DEF_LIBS)
endif

compile: $(SRC)
ifeq (,$(findstring .h, $(SRC)))
	$(CXX) -c $(OPTS) $(CXXFLAGS) $(SRC) -o $(OBJ) $(LIBS) $(DEF_LIBS)
endif

check-syntax:
	$(CXX) $(OPTS) $(CXXFLAGS) -fsyntax-only $(CHK_SOURCES)

style: $(WGET)
	./cpplint.py $(SRC) || echo done

get-lint:
	wget -q http://pc2lab.cec.miamiOH.edu/documents/cpplint.py
	chmod +x cpplint.py

many: compile-many style-many

compile-many:
ifndef EXE
	@echo Specify target executable name via command-line EXE=your_exe_name
	@exit 2
endif
	$(CXX) $(OPTS) $(CXXFLAGS) *.cpp -o $(EXE) $(LIBS) $(DEF_LIBS)

style-many: $(WGET)
	$(eval SRCS:=$(wildcard *.h *.cpp))
	./cpplint.py $(SRCS) || echo done

