// Copyright 2014 raodm@miamiOH.edu

#ifndef USER_H
#define USER_H

//-----------------------------------------------------------------
//              DO NOT MODIFY THIS FILE
//-----------------------------------------------------------------

/**
   A program to store a list of users and perform basic operations.  A
   user is represented by an User class that encapsulates the
   following information is associated with a user:
   
   - user id (one word string without any spaces)
   - first name (one word string without any spaces)
   - last name (one word string without any spaces)
*/

#include <iostream>
#include <string>

/**
   For this exercise you are expected to implement various methods in
   the User class to perform the required operations.
*/

class User {
    /*
      This method must print first name, last name, and user ID to
      the given output stream. Refer to sample output for format.
      A sample implementation of this method is available in lecture
      slides.
    */
    friend std::ostream& operator<<(std::ostream& os, const User& u);

    /*
      This method must last name, first name, and user ID from the given
      input stream. Reading each input is similar to reading a word from
      std::cin but use "is" instead of "std::cin".
    */
    friend std::istream& operator>>(std::istream& is, User& u);

public:
    /**
       The default constructor. The default constructor
       merely initializes instance variables to empty strings.
    */
    User();

    /**
       The destructor (has no specific operations to perform as this
       class does not use any dynamic memory).
    */
    ~User();

    /**
       Convenience constructor to initialize various instance values
       to default initial values.
    */
    User(const std::string& userID, const std::string& firstName = "",
         const std::string& lastName = "");

    /**
       Copy constructor. This must copy the corresponding elements
       from src to this.
    */
    User(const User& src);

    /**
       Move constructor. In this class it is similar to copy
       constructor (however, if there is dynamic data then the
       pointers are directly reused rather than copied).
    */
    User(User&& src);

    /**
       The assignment operator.

       \param[in] other The source object to copy data from.

       \return This method returns a reference to this object as per
       API requirement.
    */
    User& operator=(const User& other);

    /**
       Default comparison operation (compares this and other). This
       method operates as follows:

       If last names are equal it returns the result of
       less-than-comparison of firstNames; otherwise returns
       less-than-comparison of lastName.
    */
    bool operator<(const User& other) const;

    /**
       The equality operator. This method returns true only if
       this->userID is the exactly the same as other.userID.
    */
    bool operator==(const User& other) const;

    /**
       This method returns true if the first name, last name, or
       userID of "other" (parameter) matches corresponding
       instance variables in "this".
    */
    bool operator()(const User& other) const;

private:
    /// The user-id identifier associated with this user.
    std::string userID;
    /// The user's first name.
    std::string firstName;
    /// The user's last name.
    std::string lastName;
};

#endif

