// Copyright 2014 raodm@miamiOH.edu

#include <iostream>
#include <cmath>
#include <algorithm>
#include <iterator>
#include <fstream>
#include <stdexcept>
#include <vector>
#include <string>

// Shortcut to refer to std::vector<int>
typedef std::vector<int> NumList;

/**
   Method to determine if a given number if prime.

   This method essentially checks to see if the given number has a
   factor in the range 2 <= factor <= sqrt(num).  If a factor is
   found, then the given number is not prime. Otherwise the number
   must be prime.
   
   \param[in] num The number to be checked to determine if it is prime.
   
   \return Returns true if the given num is prime. Otherwise this
   method returns false.
*/
bool isPrime(const int num) {
    for (int i = 2; (i <= sqrt(num)); i++) {
        if (num % i == 0) {
            // Found a factor.
            return false;
        }
    }
    // num is prime as it has no factors between 2 and sqrt(num)
    return true;
}

/** Count the number of prime factors for a given number.

    This method iterates over the range 1 < fact <= num to search
    for prime factors for a given number.  It uses the isPrime()
    method to check if a factor of num is indeed prime.

    \param[in] num The number whose prime factors are to be
    returned by this method.

    \return The number of prime factors of num.
*/
int getPrimeFactorCount(const int num) {
    int factCount = 0;
    for (int fact = 2; (fact <= num); fact++) {
        if ((num % fact == 0) && (isPrime(fact))) {
            factCount++;
        }
    }
    return factCount;
}

/** Convenience method to determine if num1 has more prime factors
    than num2.

    \param[in] num1 The first number whose prime factors are to be
    counted.

    \param[in] num2 The second number whose prime factors are to be
    counted.

    \return This method returns true if num1 has more prime factors
    than num2.
*/
bool hasMorePrimeFactors(const int num1, const int num2) {
    return getPrimeFactorCount(num1) > getPrimeFactorCount(num2);
}

/** Convenience method to load numbers from a given file.

    \param[in] fileName The file from which the numbers are to be
    loaded.

    \return A vector containing the numbers loaded from the file.
 */
NumList loadNumbers(const std::string& fileName) {
    std::ifstream data(fileName);
    if (!data.good()) {
        throw std::runtime_error("Invalid input file name.");
    }
    std::istream_iterator<int> numReader(data), eof;
    NumList numList;
    std::copy(numReader, eof, std::back_inserter(numList));
    return numList;
}

/** Convenience method to write numbers to a given file.

    \param[in] fileName The file name to write the numbers to.

    \param[in] numList The list of numbers to be written to specified
    output file.
*/
void writeTo(const std::string& fileName, const NumList& numList) {
    std::ofstream data(fileName);
    std::ostream_iterator<int> numWriter(data, "\n");
    std::copy(numList.begin(), numList.end(), numWriter);
}

/*
int main(int argc, char *argv[]) {
    const int count = std::stoi(argv[1]);
    NumList numList(count);
    auto random = []() { return rand() % 100000; };
    std::generate_n(numList.begin(), count, random);
    writeTo(argv[2], numList);
    return 0;
}
*/


int main(int argc, char *argv[]) {
    if (argc < 3) {
        std::cerr << "Usage: " << argv[0] << " <InputFile> <N> <OutputFile>\n";
        return 1;
    }
    NumList numList = loadNumbers(argv[1]);
    std::sort(numList.begin(), numList.end(), hasMorePrimeFactors);
    numList.resize(std::stoi(argv[2]));
    if (argc < 4) {
        std::ostream_iterator<int> numWriter(std::cout, "\n");
        std::copy(numList.begin(), numList.end(), numWriter);
    } else {
        writeTo(argv[3], numList);
    }
    return 0;
}

// End of source code
