// Copyright 2014 raodm@miamioh.edu

//-----------------------------------------------------------------
//              DO NOT MODIFY THIS FILE
//-----------------------------------------------------------------

#include <vector>
#include <iterator>
#include <algorithm>
#include <fstream>
#include <string>
#include "User.h"

void loadData(std::vector<User>& userList) {
    // Read user data from text file called user_list.txt
    std::ifstream userDB("user_list.txt");
    std::istream_iterator<User> userData(userDB);
    std::istream_iterator<User> end;
    // User information is loaded into user list.
    std::cout << "Loading user data..." << std::flush;
    std::copy(userData, end, std::back_inserter(userList));
    std::sort(userList.begin(), userList.end());
    std::cout << "Done.\nLoaded data for " << userList.size() << " users.\n";
}

void queryData(const std::vector<User>& userList) {
    std::string query;
    do {
        std::cout << "Query (list, quit, <userID>):";
        std::cin  >> query;
        if (query == "list") {
            std::copy(userList.cbegin(), userList.cend(),
                      std::ostream_iterator<User>(std::cout, "\n"));
        } else if (query != "quit") {
            // search for the user ID
            const User dummy(query, query, query);
            std::vector<User>::const_iterator entry =
                std::find(userList.cbegin(), userList.cend(), dummy);
            std::cout << "Information for userID: '" << query << "': "
                      << (entry != userList.end() ? *entry :
                          User("", "*not found*"))
                      << std::endl;
            if (entry == userList.end()) {
                std::cout << "Here are results from generic search:\n";
                std::copy_if(userList.begin(), userList.end(),
                             std::ostream_iterator<User>(std::cout, "\n"),
                             dummy);
            }
        }
    } while (query != "quit");
}

int main() {
    // User information is loaded into user list.
    std::vector<User> userList;
    loadData(userList);
    // Let the user use different commands to query for information
    // in the user List
    queryData(userList);
    return 0;
}
