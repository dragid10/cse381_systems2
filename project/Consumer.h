//
// Created by seyi on 11/29/17.
//

#ifndef PROJECT2_CPP_CONSUMER_H
#define PROJECT2_CPP_CONSUMER_H


#include <queue>
#include "BlockingQueue.h"
using namespace std;
class Consumer{
public:
     Consumer(BlockingQueue drop);
void run();
private:
    BlockingQueue drop;

};


#endif //PROJECT2_CPP_CONSUMER_H
