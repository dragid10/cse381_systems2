//
// Created by seyi on 11/29/17.
//

#ifndef PROJECT2_CPP_PRODUCER_H
#define PROJECT2_CPP_PRODUCER_H

#include "BlockingQueue.h"
using namespace std;
class Producer {
public:
    void run();

    Producer(BlockingQueue drop);

private:
    BlockingQueue drop;
    vector<string> messages;
};


#endif //PROJECT2_CPP_PRODUCER_H
