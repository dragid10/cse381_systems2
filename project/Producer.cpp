//
// Created by seyi on 11/29/17.
//

#include "Producer.h"

void Producer::run() {
    try {
        for (const auto &s : messages) {
            drop.enqueue(s);
        }
        drop.enqueue("DONE");
    } catch (exception &e) {
        cout << "Interrupted! Last one out, turn out the lights!" << endl;
    }

}

Producer::Producer(BlockingQueue drop) {
    this->drop = drop;
    messages.emplace_back("Mares eat oats");
    messages.emplace_back("Does eat oats");
    messages.emplace_back("Little lambs eat ivy");
    messages.emplace_back("Wouldn't you eat ivy too?");
}
