//
// Created by seyi on 11/29/17.
//

#ifndef PROJECT2_CPP_BLOCKINGQUEUE_H
#define PROJECT2_CPP_BLOCKINGQUEUE_H
#define synchronized(m) \
    for(std::unique_lock<std::mutex> lk(m); lk; lk.unlock())

#include <list>
#include <mutex>
#include <iostream>           // std::cout
#include <thread>             // std::thread
#include <mutex>              // std::mutex, std::unique_lock
#include <condition_variable> // std::condition_variable
#include <vector>

using namespace std;

class BlockingQueue {
private:
    std::vector<string> queue;
    int limit = 10;
    std::mutex _mutex;
    condition_variable cv;

public:
     BlockingQueue();

    void enqueue(string item);
    string dequeue();
};


#endif //PROJECT2_CPP_BLOCKINGQUEUE_H
