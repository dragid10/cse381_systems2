import java.util.Arrays;
import java.util.List;
import java.util.concurrent.BlockingQueue;

/**
 * Name: Alex Oladele
 * Unique-ID: OLADELAA
 * Date: 11/29/17
 * Project: project02
 */
class Producer
        implements Runnable {
    private BlockingQueue<String> drop;
    private List<String> messages = Arrays.asList(
            "Mares eat oats",
            "Does eat oats",
            "Little lambs eat ivy",
            "Wouldn't you eat ivy too?");

    Producer(BlockingQueue<String> d) {
        this.drop = d;
    }

    public void run() {
        try {
            for (String s : messages)
                drop.put(s);
            drop.put("DONE");
        } catch (InterruptedException intEx) {
            System.out.println("Interrupted! Last one out, turn out the lights!");
        }
    }
}
