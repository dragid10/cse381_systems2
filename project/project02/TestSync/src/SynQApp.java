import java.util.*;
import java.util.concurrent.*;

public class SynQApp {
    public static void main(String[] args) {
        BlockingQueue<String> drop = new SynchronousQueue<>();
        (new Thread(new Producer(drop))).start();
        (new Thread(new Consumer(drop))).start();
    }
}
