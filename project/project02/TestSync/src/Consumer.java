import java.util.concurrent.BlockingQueue;

/**
 * Name: Alex Oladele
 * Unique-ID: OLADELAA
 * Date: 11/29/17
 * Project: project02
 */
class Consumer
        implements Runnable {
    private BlockingQueue<String> drop;

    Consumer(BlockingQueue<String> d) {
        this.drop = d;
    }

    public void run() {
        try {
            String msg = null;
            while (!((msg = drop.take()).equals("DONE")))
                System.out.println(msg);
        } catch (InterruptedException intEx) {
            System.out.println("Interrupted! Last one out, turn out the lights!");
        }
    }
}