//
// Created by seyi on 11/29/17.
//

#include "BlockingQueue.h"


void BlockingQueue::enqueue(string item) {
//        unique_lock<mutex> lock(_mutex);
    synchronized(_mutex) {
        while (this->queue.size() == this->limit) {
            cv.wait(lk);
        }
        if (this->queue.empty()) {
            cv.notify_all();
        }
        this->queue.push_back(item);
    }

}

string BlockingQueue::dequeue() {
    synchronized(_mutex) {
        while (this->queue.empty()) {
//        wait;
            cv.wait(lk);
        }
        if (this->queue.size() == this->limit) {
            cv.notify_all();
        }
        auto firstElem = this->queue[0];
        this->queue.erase(this->queue.begin());
        return firstElem;
    }
}

BlockingQueue::BlockingQueue() = default;
