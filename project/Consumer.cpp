//
// Created by seyi on 11/29/17.
//

#include "Consumer.h"


Consumer::Consumer(BlockingQueue drop) {
    this->drop = drop;
}

void Consumer::run() {
    try {
        std::unique_lock<std::mutex> lck(mtx);
        string msg;
        while (((msg = drop.dequeue()) != "DONE")) {
            cout << msg << endl;
        }
    } catch (exception& e) {
        cout << "Interrupted! Last one out, turn out the lights!" << endl;
    }
}
