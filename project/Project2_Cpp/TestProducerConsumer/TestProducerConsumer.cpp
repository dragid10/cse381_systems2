#include <iostream>
#include <mutex>
#include <condition_variable>
#include <random>
#include <thread>
#include <queue>
#include <algorithm>
#include <zconf.h>

using namespace std;
std::mutex mtx;
std::condition_variable cv;
std::vector<string> myVec;
int NAP_TIME = 5;
bool finished = false;

// Consumer
void consume() {
    while (true) {
        std::unique_lock<std::mutex> lck(mtx);
        cv.wait(lck, [] {
            if (!myVec.empty()) {
                return true;
            } else {
                return finished;
            }
        });

        while (!myVec.empty()) {
//            Sets random sleeptime variable. Using rand() is bad
            int sleepTime = NAP_TIME * rand();

//            Outputs var to the command
            cout << "Consumer sleeping for " << sleepTime << " seconds";
            try {
//                sleeps for a random amount of time
                this_thread::sleep_for(chrono::milliseconds(sleepTime));

            } catch (exception &e) {
            }

//            Pops first element from vector and saves it to be sent out
            string msg;
            for (const auto &s : myVec) {
                msg = s;
            }
            myVec.erase(myVec.begin());

//            Ensures there was actually a value there to pop. If not empty, then get the time from now and from the msg and outputs difference
            if (!msg.empty()) {
                time_t now = time(0);
                tm *ltm = localtime(&now);
                tm *ltm2 = localtime(&msg);

                int currTime = ltm->tm_hour;
                int pastTime = ltm2->tm_hour;

                cout << "Consumer consumed " + currTime - pastTime << endl;
            }

        }
//        If finished ever occurs (which it shouldn't) then it would break out of the loop
        if (finished) {
            break;
        }
    }
}

// Producer
void produce() {
    std::lock_guard<std::mutex> lk(mtx);

//    Inf Loop
    while (true) {
//        Random sleeptime and outputs it
        int sleepTime = NAP_TIME * rand();
        cout << "Producer sleeping for " << sleepTime << " seconds" << endl;
        try {
//            sleeps for random time
            this_thread::sleep_for(chrono::milliseconds(sleepTime));
        } catch (exception &e) {
        }

//        Gets date time and prints out the val then pushes it to vector
        time_t msg = time(0);
        char *dateTime = ctime(&msg);
        cout << "Producer produced " << dateTime << endl;
        myVec.emplace_back(dateTime);

//        Notifies all other threads
        cv.notify_all();
    }
}


int main() {
//    Starts threads
    thread first(produce);
    thread second(consume);

//    Waits for this thread to die
    first.join();
    second.join();
    return 0;
}