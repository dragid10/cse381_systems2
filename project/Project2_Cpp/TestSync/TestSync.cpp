#include <iostream>
#include <mutex>
#include <condition_variable>
#include <random>
#include <thread>
#include <queue>
#include <algorithm>

using namespace std;
std::mutex mtx;
std::condition_variable cv;
std::vector<string> myVec;
bool finished = false;

// Consumer
void consume() {
//    Runs a loop
    while (true) {
//        Creates mutex lock to ensure only one thread is accessing at a time
        std::unique_lock<std::mutex> lck(mtx);
        cv.wait(lck, [] {
            if (!myVec.empty()) {
                return true;
            } else {
                return finished;
            }
        });

//        Pops a value from the queue and pritns it out
        while (!myVec.empty()) {
            cout << myVec.front() << endl;
            myVec.erase(myVec.begin());
        }
        if (finished) {
            break;
        }
    }
}

// Producer
void produce() {
//    Adds value to the vector to be popped later and notifies all threads when it is finished
    std::lock_guard<std::mutex> lk(mtx);
    myVec.emplace_back("Mares eat oats");
    myVec.emplace_back("Does eat oats");
    myVec.emplace_back("Little lambs eat ivy");
    myVec.emplace_back("Wouldn't you eat ivy too?");
    shuffle(myVec.begin(), myVec.end(), std::mt19937(std::random_device()()));
    finished = true;
    cv.notify_all();
}


int main() {
//    Runs both methods in different threads
    thread first(produce);
    thread second(consume);
//    first.join();
//    second.join();
    return 0;
}
